part of 'language_bloc.dart';

@immutable
sealed class LanguageState {}

final class LanguageLoaded extends LanguageState {
  final Locale locale;

  LanguageLoaded({required this.locale});
}
