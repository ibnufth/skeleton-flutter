import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:skeleton/app_state/entities/language.dart';

part 'language_event.dart';
part 'language_state.dart';

class LanguageBloc extends Bloc<LanguageEvent, LanguageState> {
  LanguageBloc()
      : super(LanguageLoaded(locale: Locale(Languages.languages.first.code))) {
    on<ToggleLanguageEvent>(_onToggleLanguageEvent);
  }

  FutureOr<void> _onToggleLanguageEvent(
      ToggleLanguageEvent event, Emitter<LanguageState> emit) {
    emit(LanguageLoaded(locale: Locale(event.language.code)));
  }
}
