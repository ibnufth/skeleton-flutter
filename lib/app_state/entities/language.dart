class LanguageEntity {
  final String code;
  final String value;

  const LanguageEntity({
    required this.code,
    required this.value,
  });
}

class Languages {
  const Languages._();

  static const languages = [
    LanguageEntity(code: 'id', value: 'Bahasa Indonesia'),
    LanguageEntity(code: 'en', value: 'English'),
  ];
}
