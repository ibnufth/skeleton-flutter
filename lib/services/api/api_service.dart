// coverage:ignore-file
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:skeleton/core/exceptions.dart';
import 'package:skeleton/services/api/api.dart';

class ApiService {
  static ApiService? _instance;

  Api api = Api();

  factory ApiService() => _instance ?? ApiService._();

  ApiService._();

  Future<Response<T>> get<T>(
    String url, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
  }) async {
    bool hasInternet = await InternetConnection().hasInternetAccess;
    if (hasInternet) {
      return await api.dio
          .get<T>(url, queryParameters: queryParameters, data: data);
    }
    throw NoInternetConnectionException(RequestOptions(),
        "Tidak ada koneksi internet, pastikan perangkat anda terhubung ke internet dan coba lagi.");
  }

  Future<Response<T>> post<T>(String url,
      {dynamic data,
      Map<String, dynamic>? queryParameters,
      Options? options}) async {
    bool hasInternet = await InternetConnection().hasInternetAccess;
    if (hasInternet) {
      return await api.dio.post(url,
          data: data, queryParameters: queryParameters, options: options);
    }
    throw NoInternetConnectionException(RequestOptions(),
        "Tidak ada koneksi internet, pastikan perangkat anda terhubung ke internet dan coba lagi.");
  }

  void useAccessToken(String? token) {
    if (token != null && token.isNotEmpty) {
      api.dio.options.headers["Authorization"] = "Bearer $token";
      api.dio.options.headers[HttpHeaders.contentTypeHeader] =
          "application/json";
    } else {
      api.dio.options.headers.remove(HttpHeaders.authorizationHeader);
    }
  }
}
