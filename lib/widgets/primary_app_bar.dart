// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/reference/assets.gen.dart';

class PrimaryAppBar extends StatefulWidget implements PreferredSizeWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const PrimaryAppBar({super.key, required this.scaffoldKey});

  @override
  State<PrimaryAppBar> createState() => _PrimaryAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight((80));
}

class _PrimaryAppBarState extends State<PrimaryAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 80,
      backgroundColor: AppTheme.colors.baseWhite,
      elevation: 0.0,
      leading: Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: SvgPicture.asset(Assets.images.logomark),
      ),
      actions: [
        InkWell(
          onTap: () {
            widget.scaffoldKey.currentState?.openEndDrawer();
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: SizedBox(
                height: 40,
                width: 40,
                child: SvgPicture.asset(
                  Assets.icons.user,
                )),
          ),
        ),
      ],
      // actions: [
      //   PopupMenuButton(
      //     // add icon, by default "3 dot" icon
      //     // icon: Icon(Icons.book)
      //     itemBuilder: (context) {
      //       return [
      //         const PopupMenuItem<int>(
      //           value: 0,
      //           child: Text("My Account"),
      //         ),
      //         const PopupMenuItem<int>(
      //           value: 1,
      //           child: Text("Settings"),
      //         ),
      //         const PopupMenuItem<int>(
      //           value: 2,
      //           child: Text("Logout"),
      //         ),
      //       ];
      //     },
      //     onSelected: (value) {
      //       if (value == 0) {
      //         print("My account menu is selected.");
      //       } else if (value == 1) {
      //         print("Settings menu is selected.");
      //       } else if (value == 2) {
      //         print("Logout menu is selected.");
      //       }
      //     },
      //     child: Padding(
      //       padding: const EdgeInsets.only(right: 16.0),
      //       child: SvgPicture.asset(
      //         Assets.icons.burgerMenuIcon,
      //       ),
      //     ),
      //   ),
      // ],
    );
  }
}
