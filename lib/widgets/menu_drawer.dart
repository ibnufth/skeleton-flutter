// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/reference/assets.gen.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
          child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                TextButton.icon(
                  onPressed: () {
                    //
                  },
                  icon: SvgPicture.asset(
                    Assets.icons.home,
                    colorFilter: ColorFilter.mode(
                        AppTheme.colors.primary500, BlendMode.srcIn),
                  ),
                  label: const Text("Home"),
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextButton.icon(
                  onPressed: () {
                    //
                  },
                  icon: SvgPicture.asset(
                    Assets.icons.settings,
                  ),
                  label: const Text("Pengaturan"),
                ),
              ],
            )
          ],
        ),
      )),
    );
  }
}
