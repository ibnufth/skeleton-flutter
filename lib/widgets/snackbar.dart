// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/reference/assets.gen.dart';

SnackBar successSnackbar(
        {required String message, required BuildContext context}) =>
    SnackBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      content: Padding(
        padding: const EdgeInsets.all(8),
        child: Container(
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
              color: AppTheme.colors.success25,
              border: Border.all(color: AppTheme.colors.success300),
              borderRadius: BorderRadius.circular(4)),
          child: Row(
            children: [
              Lottie.asset(Assets.lottie.check, width: 30, height: 30),
              const SizedBox(width: 12),
              SizedBox(
                width: MediaQuery.of(context).size.width * .6,
                child: Text(
                  message,
                  maxLines: 3,
                  style: AppTheme.textStyle.textSMMedium
                      .copyWith(color: AppTheme.colors.success700),
                ),
              )
            ],
          ),
        ),
      ),
    );
SnackBar errorSnackbar(
        {required String message, required BuildContext context}) =>
    SnackBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      content: Padding(
        padding: const EdgeInsets.all(8),
        child: Container(
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
              color: AppTheme.colors.error25,
              border: Border.all(color: AppTheme.colors.error300),
              borderRadius: BorderRadius.circular(4)),
          child: Row(
            children: [
              Lottie.asset(Assets.lottie.warningRed, width: 30, height: 30),
              const SizedBox(width: 12),
              SizedBox(
                width: MediaQuery.of(context).size.width * .6,
                child: Text(
                  message,
                  maxLines: 3,
                  style: AppTheme.textStyle.textSMMedium
                      .copyWith(color: AppTheme.colors.error700),
                ),
              )
            ],
          ),
        ),
      ),
    );
