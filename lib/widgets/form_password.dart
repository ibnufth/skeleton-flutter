// ignore_for_file: public_member_api_docs, sort_constructors_first
// coverage:ignore-file

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/reference/assets.gen.dart';

class FormPassword extends StatefulWidget {
  final String name;
  final String? hintText;
  final bool isConfirmation;
  final String? parentPassword;
  final AutovalidateMode? autovalidateMode;
  final void Function(String? value)? onChanged;
  final String? Function(String?)? validators;
  const FormPassword({
    Key? key,
    required this.name,
    this.hintText,
    this.isConfirmation = false,
    this.parentPassword,
    this.onChanged,
    this.autovalidateMode,
    this.validators,
  }) : super(key: key);

  @override
  State<FormPassword> createState() => _FormPasswordState();
}

class _FormPasswordState extends State<FormPassword> {
  bool isShow = false;

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: widget.name,
      obscureText: !isShow,
      onChanged: widget.onChanged,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(),
        if (widget.isConfirmation)
          FormBuilderValidators.equal(widget.parentPassword ?? "",
              errorText: "Konfirmasi kata sandi tidak sesuai"),
        if (widget.validators != null) widget.validators!
      ]),
      autovalidateMode: widget.autovalidateMode,
      decoration: InputDecoration(
          errorMaxLines: 2,
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                isShow = !isShow;
              });
            },
            icon: SvgPicture.asset(
              isShow ? Assets.icons.eyeAlt : Assets.icons.eyeClose,
              colorFilter:
                  ColorFilter.mode(AppTheme.colors.primary600, BlendMode.srcIn),
            ),
          ),
          hintText: widget.hintText,
          floatingLabelBehavior: FloatingLabelBehavior.never),
    );
  }
}
