import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:skeleton/app_state/bloc/language_bloc.dart';
import 'package:skeleton/app_state/entities/language.dart';
import 'package:skeleton/core/locator.dart';
import 'package:skeleton/core/routes.dart';
import 'package:skeleton/services/global_key/global_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  var initialRoute = await Routes.initialRoute;
  runApp(MyApp(
    initialRoute: initialRoute,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.initialRoute});
  final String initialRoute;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LanguageBloc(),
      child: BlocBuilder<LanguageBloc, LanguageState>(
        builder: (context, state) {
          if (state is LanguageLoaded) {
            return MaterialApp(
              title: 'Flutter Skeleton',
              scaffoldMessengerKey: getIt<GlobalKeyService>().scaffoldKey,
              theme: ThemeData(
                colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                useMaterial3: true,
              ),
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              locale: state.locale,
              supportedLocales: AppLocalizations.supportedLocales,
              initialRoute: initialRoute,
              routes: Routes.routes,
              debugShowCheckedModeBanner: false,
            );
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        actions: [
          BlocProvider.value(
            value: LanguageBloc(),
            child: IconButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (ctx) {
                    return Dialog(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: Languages.languages
                            .map(
                              (e) => SizedBox(
                                width: MediaQuery.of(context).size.width * .6,
                                child: ListTile(
                                  onTap: () {
                                    context
                                        .read<LanguageBloc>()
                                        .add(ToggleLanguageEvent(e));
                                    Navigator.pop(context);
                                  },
                                  title: Text(e.value),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    );
                  },
                );
              },
              icon: const Icon(Icons.settings_rounded),
            ),
          )
        ],
        title: Text("${AppLocalizations.of(context)?.iamSkeleton}"),
      ),
      body: Center(
        child: Text(
            "${AppLocalizations.of(context)?.helloWorld} ${AppLocalizations.of(context)?.iamSkeleton}"),
      ),
    );
  }
}
