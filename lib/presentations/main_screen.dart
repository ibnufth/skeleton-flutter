// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/presentations/home/home_screen.dart';
import 'package:skeleton/reference/assets.gen.dart';
import 'package:skeleton/widgets/menu_drawer.dart';
import 'package:skeleton/widgets/primary_app_bar.dart';

class MainScreen extends StatefulWidget {
  static const route = "main";
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int currentPageIndex = 0;

  void changePage(int page) {
    setState(() {
      currentPageIndex = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: PrimaryAppBar(scaffoldKey: scaffoldKey),
      endDrawer: const MenuDrawer(),
      bottomNavigationBar: navigation(),
      body: [
        const Padding(
          padding: EdgeInsets.only(),
          child: HomeScreen(),
        ),
        Container(
          color: Colors.black12,
        ),
        Container(
          color: Colors.blue[100],
        ),
        Container(
          color: Colors.amber[100],
        ),
        Container(
          color: Colors.green[100],
        ),
      ][currentPageIndex],
    );
  }

  Widget navigation() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            decoration: BoxDecoration(
                color: currentPageIndex == 0
                    ? AppTheme.colors.primary50
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(4)),
            child: IconButton(
              enableFeedback: false,
              onPressed: () {
                changePage(0);
              },
              icon: SvgPicture.asset(
                Assets.icons.home,
                colorFilter: ColorFilter.mode(
                    currentPageIndex == 0
                        ? AppTheme.colors.primary500
                        : AppTheme.colors.gray500,
                    BlendMode.srcIn),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            decoration: BoxDecoration(
                color: currentPageIndex == 1
                    ? AppTheme.colors.primary50
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(4)),
            child: IconButton(
              enableFeedback: false,
              onPressed: () {
                changePage(1);
              },
              icon: SvgPicture.asset(
                Assets.icons.switchHorizontal,
                colorFilter: ColorFilter.mode(
                    currentPageIndex == 1
                        ? AppTheme.colors.primary500
                        : AppTheme.colors.gray500,
                    BlendMode.srcIn),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 6,
            ),
            decoration: BoxDecoration(
              color: currentPageIndex == 2
                  ? AppTheme.colors.primary50
                  : Colors.transparent,
              borderRadius: BorderRadius.circular(4),
            ),
            child: IconButton(
              enableFeedback: false,
              onPressed: () {
                changePage(2);
              },
              icon: SvgPicture.asset(
                Assets.icons.clipboard,
                colorFilter: ColorFilter.mode(
                    currentPageIndex == 2
                        ? AppTheme.colors.primary500
                        : AppTheme.colors.gray500,
                    BlendMode.srcIn),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            decoration: BoxDecoration(
                color: currentPageIndex == 3
                    ? AppTheme.colors.primary50
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(4)),
            child: IconButton(
              enableFeedback: false,
              onPressed: () {
                changePage(3);
              },
              icon: SvgPicture.asset(
                Assets.icons.clock,
                colorFilter: ColorFilter.mode(
                    currentPageIndex == 3
                        ? AppTheme.colors.primary500
                        : AppTheme.colors.gray500,
                    BlendMode.srcIn),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
            decoration: BoxDecoration(
                color: currentPageIndex == 4
                    ? AppTheme.colors.primary50
                    : Colors.transparent,
                borderRadius: BorderRadius.circular(4)),
            child: IconButton(
              enableFeedback: false,
              onPressed: () {
                changePage(4);
              },
              icon: SvgPicture.asset(
                Assets.icons.database,
                colorFilter: ColorFilter.mode(
                    currentPageIndex == 4
                        ? AppTheme.colors.primary500
                        : AppTheme.colors.gray500,
                    BlendMode.srcIn),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
