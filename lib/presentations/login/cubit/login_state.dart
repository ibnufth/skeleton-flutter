part of 'login_cubit.dart';

@immutable
sealed class LoginState extends Equatable {}

final class LoginInitial extends LoginState {
  @override
  List<Object?> get props => [];
}

final class LoginLoading extends LoginState {
  @override
  List<Object?> get props => [];
}

final class LoginSuccess extends LoginState {
  @override
  List<Object?> get props => [];
}

final class LoginError extends LoginState {
  final String message;

  LoginError(this.message);
  @override
  List<Object?> get props => [message];
}
