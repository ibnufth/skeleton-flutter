import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit(super.initialState);

  // final LoginUseCase _loginUseCase;

  // LoginCubit({LoginUseCase? loginUseCase}) // coverage:ignore-start
  //     : _loginUseCase = loginUseCase ?? getIt<LoginUseCase>(),
  //       super(LoginInitial());
  // // coverage:ignore-end
  Future<void> login({
    required String username,
    required String password,
    void Function()? onSuccess,
  }) async {
    emit(LoginLoading());
    // final result =
    // await _loginUseCase(LoginParam(username: username, password: password));
    // result.fold((l) {
    //   log("left ${l.message}");
    //   emit(LoginError(l.message));
    // }, (r) {
    //   onSuccess?.call();
    //   emit(LoginSuccess());
    // });
    emit(LoginSuccess());
  }
}
