// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:skeleton/core/theme.dart';
import 'package:skeleton/reference/assets.gen.dart';
import 'package:skeleton/widgets/buttons.dart';
import 'package:skeleton/widgets/form_password.dart';

import 'cubit/login_cubit.dart';

class LoginScreen extends StatefulWidget {
  static const route = "login";
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var formKey = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
        body: Center(
      child: Container(
        width: screenSize.width,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: BlocListener<LoginCubit, LoginState>(
          listener: (context, state) {
            if (state is LoginSuccess) {
              // Navigator.of(context).pushNamed(OTPVerificationScreen.route);
            }
          },
          child: BlocBuilder<LoginCubit, LoginState>(
            builder: (context, state) {
              return SafeArea(
                child: FormBuilder(
                  key: formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SvgPicture.asset(Assets.images.logomark),
                        const SizedBox(height: 80),
                        Text(
                          "Masuk ke Akun Anda",
                          style: AppTheme.textStyle.displayMDSemibold,
                        ),
                        const SizedBox(height: 12),
                        Text(
                          "Selamat datang kembali! Silakan masukkan alamat email dan kata sandi Anda.",
                          style: AppTheme.textStyle.textMDRegular,
                        ),
                        const SizedBox(height: 32),
                        if (state is LoginError)
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                state.message,
                                style:
                                    TextStyle(color: AppTheme.colors.error600),
                              ),
                              const SizedBox(height: 16)
                            ],
                          ),
                        Text(
                          "Email",
                          style: AppTheme.textStyle.textSMMedium,
                        ),
                        const SizedBox(height: 6.0),
                        FormBuilderTextField(
                          name: "username",
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(),
                            FormBuilderValidators.email(),
                          ]),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: const InputDecoration(
                            hintText: "Masukkan email Anda",
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          "Kata sandi",
                          style: AppTheme.textStyle.textSMMedium,
                        ),
                        const SizedBox(height: 6.0),
                        const FormPassword(
                          name: 'password',
                          hintText: "Masukan kata sandi Anda",
                        ),
                        Row(
                          children: [
                            const Spacer(),
                            TextButton(
                              onPressed: () {
                                //
                                formKey.currentState?.reset();
                              },
                              child: const Text(
                                "Lupa Password",
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: PrimaryButton(
                            isLoading: state is LoginLoading,
                            onPressed: () {
                              if (formKey.currentState?.saveAndValidate() ??
                                  false) {
                                context.read<LoginCubit>().login(
                                    username: formKey.currentState
                                        ?.fields['username']?.value,
                                    password: formKey.currentState
                                        ?.fields['password']?.value,
                                    onSuccess: () {
                                      formKey.currentState?.reset();
                                    });
                              }
                            },
                            child: const Text("Masuk"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    ));
  }
}
