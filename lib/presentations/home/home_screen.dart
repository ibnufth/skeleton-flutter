// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:skeleton/core/theme.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppTheme.colors.primary600,
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Selamat datang,",
                style: AppTheme.textStyle.displayXSSemibold,
              ),
              const SizedBox(height: 4),
              Text(
                "Terima kasih telah memilih DMS untuk mengoptimalkan pekerjaan Anda.",
                style: TextStyle(color: AppTheme.colors.gray600),
              ),
              const SizedBox(height: 32),
            ],
          ),
        ),
      ),
    );
  }
}

class ChartData {
  ChartData(this.x, this.y);

  final String x;
  final double y;
}
