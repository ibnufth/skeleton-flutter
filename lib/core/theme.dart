// ignore_for_file: unused_field
// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  static const colors = _AppColors();
  static const textStyle = _DMSFont();
  static const buttonStyle = _AppButtonStyle();
}

class _AppButtonStyle {
  const _AppButtonStyle();
  ButtonStyle get filledStyle => FilledButton.styleFrom(
        minimumSize: const Size(28, 44),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        textStyle: AppTheme.textStyle.textMDSemibold,
      );
  ButtonStyle get outlinedStyle => OutlinedButton.styleFrom(
        backgroundColor: AppTheme.colors.baseWhite,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: AppTheme.colors.gray300),
          borderRadius: BorderRadius.circular(4),
        ),
      );
}

class _AppColors {
  const _AppColors();
  //color
  Color get baseWhite => const Color.fromARGB(255, 255, 255, 255);
  Color get baseBlack => const Color.fromARGB(255, 0, 0, 0);
  Color get gray25 => const Color.fromARGB(255, 252, 252, 253);
  Color get gray50 => const Color.fromARGB(255, 249, 249, 251);
  Color get gray100 => const Color.fromARGB(255, 239, 241, 245);
  Color get gray200 => const Color.fromARGB(255, 220, 223, 234);
  Color get gray300 => const Color.fromARGB(255, 185, 192, 212);
  Color get gray400 => const Color.fromARGB(255, 125, 137, 175);
  Color get gray500 => const Color.fromARGB(255, 93, 107, 152);
  Color get gray600 => const Color.fromARGB(255, 74, 85, 120);
  Color get gray700 => const Color.fromARGB(255, 64, 73, 104);
  Color get gray800 => const Color.fromARGB(255, 48, 55, 79);
  Color get gray900 => const Color.fromARGB(255, 17, 19, 34);
  Color get gray950 => const Color.fromARGB(255, 14, 16, 27);
  Color get primary25 => const Color.fromARGB(255, 252, 252, 253);
  Color get primary50 => const Color.fromARGB(255, 247, 249, 253);
  Color get primary100 => const Color.fromARGB(255, 233, 235, 247);
  Color get primary200 => const Color.fromARGB(255, 210, 215, 239);
  Color get primary300 => const Color.fromARGB(255, 171, 178, 226);
  Color get primary400 => const Color.fromARGB(255, 118, 127, 183);
  Color get primary500 => const Color.fromARGB(255, 60, 79, 185);
  Color get primary600 => const Color.fromARGB(255, 52, 64, 141);
  Color get primary700 => const Color.fromARGB(255, 45, 56, 124);
  Color get primary800 => const Color.fromARGB(255, 33, 42, 95);
  Color get primary900 => const Color.fromARGB(255, 18, 20, 33);
  Color get primary950 => const Color.fromARGB(255, 10, 13, 31);
  Color get error25 => const Color.fromARGB(255, 255, 251, 250);
  Color get error50 => const Color.fromARGB(255, 254, 243, 242);
  Color get error100 => const Color.fromARGB(255, 254, 228, 226);
  Color get error200 => const Color.fromARGB(255, 254, 205, 202);
  Color get error300 => const Color.fromARGB(255, 253, 162, 155);
  Color get error400 => const Color.fromARGB(255, 249, 112, 102);
  Color get error500 => const Color.fromARGB(255, 240, 68, 56);
  Color get error600 => const Color.fromARGB(255, 217, 45, 32);
  Color get error700 => const Color.fromARGB(255, 180, 35, 24);
  Color get error800 => const Color.fromARGB(255, 145, 32, 24);
  Color get error900 => const Color.fromARGB(255, 122, 39, 26);
  Color get error950 => const Color.fromARGB(255, 85, 22, 12);
  Color get warning25 => const Color.fromARGB(255, 255, 252, 245);
  Color get warning50 => const Color.fromARGB(255, 255, 250, 235);
  Color get warning100 => const Color.fromARGB(255, 254, 240, 199);
  Color get warning200 => const Color.fromARGB(255, 254, 223, 137);
  Color get warning300 => const Color.fromARGB(255, 254, 200, 75);
  Color get warning400 => const Color.fromARGB(255, 253, 176, 34);
  Color get warning500 => const Color.fromARGB(255, 247, 144, 9);
  Color get warning600 => const Color.fromARGB(255, 220, 104, 3);
  Color get warning700 => const Color.fromARGB(255, 181, 71, 8);
  Color get warning800 => const Color.fromARGB(255, 147, 55, 13);
  Color get warning900 => const Color.fromARGB(255, 122, 46, 14);
  Color get warning950 => const Color.fromARGB(255, 78, 29, 9);
  Color get success25 => const Color.fromARGB(255, 246, 254, 249);
  Color get success50 => const Color.fromARGB(255, 236, 253, 243);
  Color get success100 => const Color.fromARGB(255, 220, 250, 230);
  Color get success200 => const Color.fromARGB(255, 171, 239, 198);
  Color get success300 => const Color.fromARGB(255, 117, 224, 167);
  Color get success400 => const Color.fromARGB(255, 71, 205, 137);
  Color get success500 => const Color.fromARGB(255, 23, 178, 106);
  Color get success600 => const Color.fromARGB(255, 7, 148, 85);
  Color get success700 => const Color.fromARGB(255, 6, 118, 71);
  Color get success800 => const Color.fromARGB(255, 8, 93, 58);
  Color get success900 => const Color.fromARGB(255, 7, 77, 49);
  Color get success950 => const Color.fromARGB(255, 5, 51, 33);
}

class _DMSFont {
  const _DMSFont();
  TextStyle get _fontPlusJakarta => GoogleFonts.plusJakartaSans();
  TextStyle get _fontEpilogue => GoogleFonts.epilogue();
  TextStyle get display2xlRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 72,
        fontWeight: FontWeight.w400,
      );

  TextStyle get display2xlMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 72,
        fontWeight: FontWeight.w500,
      );

  TextStyle get display2xlSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 72,
        fontWeight: FontWeight.w600,
      );

  TextStyle get display2xlBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 72,
        fontWeight: FontWeight.w900,
      );

  TextStyle get displayXLRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 60,
        fontWeight: FontWeight.w400,
      );

  TextStyle get displayXLMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 60,
        fontWeight: FontWeight.w500,
      );

  TextStyle get displayXLSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 60,
        fontWeight: FontWeight.w600,
      );

  TextStyle get displayXLBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 60,
        fontWeight: FontWeight.w900,
      );

  TextStyle get displayLGRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 48,
        fontWeight: FontWeight.w400,
      );

  TextStyle get displayLGMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 48,
        fontWeight: FontWeight.w500,
      );

  TextStyle get displayLGSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 48,
        fontWeight: FontWeight.w600,
      );

  TextStyle get displayLGBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 48,
        fontWeight: FontWeight.w900,
      );
  TextStyle get displayMDRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 36,
        fontWeight: FontWeight.w400,
      );

  TextStyle get displayMDMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 36,
        fontWeight: FontWeight.w500,
      );

  TextStyle get displayMDSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 36,
        fontWeight: FontWeight.w600,
      );

  TextStyle get displayMDBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 36,
        fontWeight: FontWeight.w900,
      );

  TextStyle get displaySMRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 30,
        fontWeight: FontWeight.w400,
      );

  TextStyle get displaySMMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 30,
        fontWeight: FontWeight.w500,
      );

  TextStyle get displaySMSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 30,
        fontWeight: FontWeight.w600,
      );

  TextStyle get displaySMBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 30,
        fontWeight: FontWeight.w900,
      );

  TextStyle get displaySMMediumItalic => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 30,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
      );

  TextStyle get displayXSRegular => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 24,
        fontWeight: FontWeight.w400,
      );

  TextStyle get displayXSMedium => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 24,
        fontWeight: FontWeight.w500,
      );

  TextStyle get displayXSSemibold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 24,
        fontWeight: FontWeight.w600,
      );

  TextStyle get displayXSBold => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 24,
        fontWeight: FontWeight.w900,
      );

  TextStyle get displayXSMediumItalic => _fontEpilogue.copyWith(
        color: const _AppColors().gray900,
        fontSize: 24,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textXLRegular => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w400,
      );

  TextStyle get textXLMedium => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w500,
      );

  TextStyle get textXLSemibold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w600,
      );

  TextStyle get textXLBold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w900,
      );

  TextStyle get textXLRegularItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textXLMediumItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textXLSemiboldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textXLBoldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textXLRegularUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 20,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
      );

  TextStyle get textLGRegular => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w400,
      );

  TextStyle get textLGMedium => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w500,
      );

  TextStyle get textLGSemibold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w600,
      );

  TextStyle get textLGBold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w900,
      );

  TextStyle get textLGRegularItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textLGMediumItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textLGSemiboldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textLGBoldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textLGRegularUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
      );

  TextStyle get textLGMediumUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 18,
        fontWeight: FontWeight.w500,
        decoration: TextDecoration.underline,
      );

  TextStyle get textMDRegular => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w400,
      );

  TextStyle get textMDMedium => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w500,
      );

  TextStyle get textMDSemibold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w600,
      );

  TextStyle get textMDBold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w900,
      );

  TextStyle get textMDRegularItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textMDMediumItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textMDSemiboldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textMDBoldItalic => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.italic,
      );

  TextStyle get textMDRegularUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
      );

  TextStyle get textMDMediumUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 16,
        fontWeight: FontWeight.w500,
        decoration: TextDecoration.underline,
      );

  TextStyle get textSMRegular => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w400,
      );

  TextStyle get textSMMedium => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w500,
      );

  TextStyle get textSMSemibold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      );

  TextStyle get textSMBold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w900,
      );

  TextStyle get textSMRegularUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        decoration: TextDecoration.underline,
      );

  TextStyle get textSMMediumUnderlined => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        decoration: TextDecoration.underline,
      );

  TextStyle get textXSRegular => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 12,
        fontWeight: FontWeight.w400,
      );

  TextStyle get textXSMedium => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 12,
        fontWeight: FontWeight.w500,
      );

  TextStyle get textXSSemibold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 12,
        fontWeight: FontWeight.w600,
      );

  TextStyle get textXSBold => TextStyle(
        color: const _AppColors().gray600,
        fontSize: 12,
        fontWeight: FontWeight.w900,
      );
}
