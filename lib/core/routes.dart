// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:skeleton/core/locator.dart';
import 'package:skeleton/presentations/login/login_screen.dart';
import 'package:skeleton/presentations/main_screen.dart';
import 'package:skeleton/reference/constant.dart';

class Routes {
  static final FlutterSecureStorage _storage = getIt<FlutterSecureStorage>();
  static Future<String> get initialRoute async {
    final token = await _storage.read(key: cToken);
    if (token == null) {
      return LoginScreen.route;
    } else {
      return MainScreen.route;
    }
  }

  static Map<String, WidgetBuilder> get routes => {
        //Example
        // HomePage.route: (_) => HomePage(),
        // DetailsPage.route: (context) {
        //   var argument = ModalRoute.of(context).settings.arguments;
        //   return DetailsPage(
        //     data_1: argument[DetailsPage.data_1],
        //   );
        // }
        LoginScreen.route: (context) => const LoginScreen(),
        MainScreen.route: (context) => const MainScreen(),
      };
}
