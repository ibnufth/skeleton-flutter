// coverage:ignore-file
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:skeleton/services/api/api_service.dart';
import 'package:skeleton/services/global_key/global_service.dart';

final getIt = GetIt.instance;
Future<void> configureDependencies() async {
  getIt.registerSingleton(Dio());
  getIt.registerSingleton(const FlutterSecureStorage());
  getIt.registerSingletonAsync<SharedPreferences>(
      () => SharedPreferences.getInstance());
  await getIt.isReady<SharedPreferences>();
  getIt.registerSingleton(DeviceInfoPlugin());
  getIt.registerLazySingleton(() => GlobalKeyService());
  getIt.registerSingleton(InternetConnection());

  //Service
  getIt.registerSingleton(ApiService());

  //Data Source

  //Repository

  //UseCase

  //State
}
